using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using XDemo;
using XDemo.Droid;
using Xamarin.Auth;
using Microsoft.WindowsAzure.MobileServices;

[assembly: ExportRenderer(typeof(LoginPage), typeof(LoginPageRenderer))]

namespace XDemo.Droid
{
    public class LoginPageRenderer : PageRenderer
    {
        protected override async void OnElementChanged(ElementChangedEventArgs<Page> e)
        {
            base.OnElementChanged(e);

            // this is a ViewGroup - so should be able to load an AXML file and FindView<>
            var activity = this.Context as Activity;

            var auth = new OAuth1Authenticator(
                consumerKey: Constants.TwitterApiKey,
                consumerSecret: Constants.TwitterApiSecret,
                requestTokenUrl: new Uri("https://api.twitter.com/oauth/request_token"),
                authorizeUrl: new Uri("https://api.twitter.com/oauth/authorize/"),
                accessTokenUrl: new Uri(" https://api.twitter.com/oauth/access_token"),
                callbackUrl: new Uri("http://mobile.twitter.com")
                );

            auth.Completed += (sender, eventArgs) => {
                if (eventArgs.IsAuthenticated)
                {
                    // Use eventArgs.Account to do wonderful things
                    App.SaveToken(eventArgs.Account.Properties["oauth_token"]);
                    App.SaveConsumerKey(eventArgs.Account.Properties["oauth_consumer_key"]);
                    App.SaveConsumerSecret(eventArgs.Account.Properties["oauth_consumer_secret"]);
                    App.SaveUser(eventArgs.Account.Properties["screen_name"]);
                    App.SaveUserId(eventArgs.Account.Properties["user_id"]);

                    App.SuccessfulLoginAction.Invoke();
                }
                else
                {
                    // The user cancelled
                }
            };

            activity.StartActivity(auth.GetUI(activity));
        }
    }
}