﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDemo.UITests
{
    class MainPage : BasePage
    {
        string BindingButtonTitle = "Binding";

        public MainPage() : base("Binding", "Binding")
        {

        }

        public MainPage ClickBindingButton()
        {
            app.WaitForElement(BindingButtonTitle);
            app.Screenshot("Tapping on Binding");
            app.Tap(BindingButtonTitle);
            return this;
        }
    }
}
