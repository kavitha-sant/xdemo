﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;

namespace XDemo.UITests
{
    public class BindingPage : BasePage
    {

        public BindingPage()
            : base(x => x.Id("toolbar").Child(1).Text("Square Root"), x => x.Class("UINavigationBar").Id("Square Root"))
        {
        }

        public BindingPage EnterNumber()
        {
            Query inputNumber = x => x.Marked("InputNumber").Index(0);

            app.WaitForElement(inputNumber);
            app.ClearText(inputNumber);
            app.EnterText(inputNumber, "144");

            return this;
        }

        public BindingPage ClickCalculate()
        {
            Query calculateButton = x => x.Text("Calculate Square Root").Index(0);

            app.WaitForElement(calculateButton);
            app.Tap(calculateButton);

            return this;
        }

        public BindingPage VerifySquareRoot()
        {
            Query outputNumber = x => x.Marked("OutputNumber").Index(0);

            app.WaitForElement(outputNumber);

            string result = app.Query(outputNumber)[0].Text;
            Assert.IsTrue(result.Equals("12"), "Square root not calculated");

            app.Screenshot("Square Root calculated.");

            return this;
        }
    }
}
