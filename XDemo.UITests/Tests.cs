﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;
using Xamarin.UITest.Android;
using Xamarin.UITest.iOS;

namespace XDemo.UITests
{
    [TestFixture(Platform.Android)]
    public class Tests
    {
        IApp app;
        Platform platform;
        protected bool OnAndroid;
        protected bool OniOS;

        public Tests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
            OnAndroid = app.GetType() == typeof(AndroidApp);
            OniOS = app.GetType() == typeof(iOSApp);
        }

        [Test]
        public void AppLaunches()
        {
            app.Screenshot("First screen.");
        }

        [Test]
        [Category("UITest")]
        public void BindingPage_andBack()
        {
            new MainPage()
                .ClickBindingButton();

            new BindingPage()
                .EnterNumber()
                .ClickCalculate()
                .VerifySquareRoot();

            AppInitializer.App.Back();

            AppInitializer.App.Back();
        }

    }
}

