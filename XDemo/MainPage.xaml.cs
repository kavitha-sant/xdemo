﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XDemo.ViewModels;
using XDemo.Views;

namespace XDemo
{
    public partial class MainPage : ContentPage
    {
        public ICommand SquareRootCommand { get; private set; }

        public MainPage()
        {
            InitializeComponent();

            var btnSqrRoot = new Button
            {
                Text = "Binding",
                Style = Application.Current.Resources["bindingButton"] as Style
            };

            btnSqrRoot.Clicked += OnSquareRootButtonChanged;

            var btnColorPref = new Button
            {
                Text = "Database",
                Style = Application.Current.Resources["bindingButton"] as Style
            };

            btnColorPref.Clicked += OnColorPrefClicked;

            var btnTwitter = new Button
            {
                Image = "twitter.png",
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            btnTwitter.Clicked += OnTwitterClicked;

            var stackLayout = new StackLayout
            {
                VerticalOptions = LayoutOptions.Center,
                Children = {btnSqrRoot, btnColorPref,btnTwitter}
            };

            this.Content = stackLayout;
        }

        private void OnSquareRootButtonChanged(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new SquareRootPage
            {
                Title = "Square Root",
                BindingContext = new SquareRootViewModel()
            });
        }

        private void OnColorPrefClicked(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new ColorPrefPage
            {
                Title = "Color Preferences",
                BindingContext = new ColorPrefViewModel()
            });
        }

        private async void OnTwitterClicked(object sender, EventArgs e)
        {
            if (!App.IsLoggedIn)
            {
                await Navigation.PushModalAsync(new LoginPage());
            }
            else
            {
                await Navigation.PushAsync(new TwitterProfilePage
                {
                    Title = "Twitter Profile",
                    BindingContext = new TwitterProfileViewModel(App.User, App.UserId, App.ConsumerKey, App.ConsumerSecret)
                });
            }
        }
    }

    public class LoginPage : ContentPage
    {

    }
}
