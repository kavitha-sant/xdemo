﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using XDemo.ViewModels;

namespace XDemo.Views
{
    public partial class ColorPrefPage : ContentPage
    {
        protected ColorPrefViewModel ViewModel => BindingContext as ColorPrefViewModel;

        public ColorPrefPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await ViewModel.ExecuteLoadColorPrefsCommandAsync();
        }
    }
}
