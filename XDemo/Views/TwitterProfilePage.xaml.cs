﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using XDemo.ViewModels;

namespace XDemo.Views
{
    public partial class TwitterProfilePage : ContentPage
    {
        protected TwitterProfileViewModel ViewModel => BindingContext as TwitterProfileViewModel;

        public TwitterProfilePage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await ViewModel.GetTwitterImages();
        }
    }
}
