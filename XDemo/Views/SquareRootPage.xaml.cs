﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using XDemo.ViewModels;

namespace XDemo.Views
{
    public partial class SquareRootPage : ContentPage
    {
        public SquareRootPage()
        {
            InitializeComponent();
        }

        private void Entry_TextChanged(object sender, TextChangedEventArgs e)
        {
            int rotation = 0;
            if(int.TryParse(e.NewTextValue, out rotation))
            {
                Resources["dynamicLabelStyle"] = new Style(typeof(Label))
                {
                    Setters =
                    {
                        new Setter { Property = Label.RotationProperty, Value = rotation },
                        new Setter { Property = Label.TextProperty, Value = string.Format($"Rotation {rotation}") }
                    }
                };
            }
        }
    }
}
