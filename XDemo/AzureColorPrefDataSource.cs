﻿using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.MobileServices.SQLiteStore;
using Microsoft.WindowsAzure.MobileServices.Sync;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XDemo.Models;

namespace XDemo
{
    public class AzureColorPrefDataSource
    {
        public string AzureAppServiceUrl = "https://evokedish.azurewebsites.net";

        public static MobileServiceClient _MobileService { get; set; }

        SyncHandler<ColorPreference> _SyncHandler;

        IMobileServiceSyncTable<ColorPreference> _ColorPrefTable;

        MobileServiceSQLiteStore _MobileServiceSQLiteStore;

        bool _IsInitialized;

        const string _LocalDbName = "colorprefs.db";

        #region some nifty exception helpers

        /// <summary>
        /// This method is intended for encapsulating the catching of exceptions related to the Azure MobileServiceClient.
        /// </summary>
        /// <param name="execute">A Func that contains the async work you'd like to do.</param>
        public static async Task Execute(Func<Task> execute)
        {
            try
            {
                await execute().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                HandleExceptions(ex);
            }
        }

        /// <summary>
        /// This method is intended for encapsulating the catching of exceptions related to the Azure MobileServiceClient.
        /// </summary>
        /// <param name="execute">A Func that contains the async work you'd like to do, and will return some value.</param>
        /// <param name="defaultReturnObject">A default return object, which will be returned in the event that an operation in the Func throws an exception.</param>
        /// <typeparam name="T">The type of the return value that the Func will returns, and also the type of the default return object. </typeparam>
        public static async Task<T> Execute<T>(Func<Task<T>> execute, T defaultReturnObject)
        {
            try
            {
                return await execute().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                HandleExceptions(ex);
            }
            return defaultReturnObject;
        }

        /// <summary>
        /// Handles the exceptions.
        /// </summary>
        /// <returns>The exceptions.</returns>
        /// <param name="ex">Ex.</param>
        public static void HandleExceptions(Exception ex)
        {
            if (ex is MobileServiceInvalidOperationException)
            {
                // TODO: report with HockeyApp
                System.Diagnostics.Debug.WriteLine($"MOBILE SERVICE ERROR {ex.Message}");
                return;
            }

            if (ex is MobileServicePushFailedException)
            {
                var pushResult = ((MobileServicePushFailedException)ex).PushResult;

                foreach (var e in pushResult.Errors)
                {
                    System.Diagnostics.Debug.WriteLine($"ERROR {pushResult.Status}: {e.RawResult}");
                }
            }

            else
            {
                // TODO: report with HockeyApp
                System.Diagnostics.Debug.WriteLine($"ERROR {ex.Message}");
            }
        }

        #endregion

        #region Data Access

        public async Task<IEnumerable<ColorPreference>> GetItemsAsync()
        {
            return await Execute<IEnumerable<ColorPreference>>(async () =>
            {
                await SyncItemsAsync().ConfigureAwait(false);
                return await _ColorPrefTable.OrderBy(x => x.Id).ToEnumerableAsync().ConfigureAwait(false);
            }, new List<ColorPreference>()).ConfigureAwait(false);
        }

        public async Task<IEnumerable<ColorPreference>> GetItemsAsync(IEnumerable<string> colorPrefIds)
        {
            if (!colorPrefIds.Any())
                return new List<ColorPreference>();

            return await Execute<IEnumerable<ColorPreference>>(async () =>
            {
                await SyncItemsAsync().ConfigureAwait(false);
                return await _ColorPrefTable.Where(x => colorPrefIds.Contains(x.Id)).OrderBy(x => x.Id).ToEnumerableAsync().ConfigureAwait(false);
            }, new List<ColorPreference>()).ConfigureAwait(false);
        }

        public async Task<ColorPreference> GetItemAsync(string id)
        {
            return await Execute<ColorPreference>(async () =>
            {
                await SyncItemsAsync().ConfigureAwait(false);
                return await _ColorPrefTable.LookupAsync(id).ConfigureAwait(false);
            }, null).ConfigureAwait(false);
        }

        public async Task<bool> AddItemAsync(ColorPreference item)
        {
            return await Execute<bool>(async () =>
            {
                await InitializeAsync().ConfigureAwait(false);
                await _ColorPrefTable.InsertAsync(item).ConfigureAwait(false);
                await SyncItemsAsync().ConfigureAwait(false);
                return true;
            }, false).ConfigureAwait(false);
        }

        public async Task<bool> UpdateItemAsync(ColorPreference item)
        {
            return await Execute<bool>(async () =>
            {
                await InitializeAsync().ConfigureAwait(false);
                await _ColorPrefTable.UpdateAsync(item).ConfigureAwait(false);
                await SyncItemsAsync().ConfigureAwait(false);
                return true;
            }, false).ConfigureAwait(false);
        }

        public async Task<bool> RemoveItemAsync(ColorPreference item)
        {
            return await Execute<bool>(async () =>
            {
                await InitializeAsync().ConfigureAwait(false);
                await _ColorPrefTable.DeleteAsync(item).ConfigureAwait(false);
                await SyncItemsAsync().ConfigureAwait(false);
                return true;
            }, false).ConfigureAwait(false);
        }

        #endregion


        #region helper methods for dealing with the state of the local store

        /// <summary>
        /// Initialize this instance.
        /// </summary>
        async Task<bool> InitializeAsync()
        {
            return await Execute<bool>(async () =>
            {
                if (_IsInitialized)
                    return true;

                _MobileService = new MobileServiceClient(AzureAppServiceUrl);

                _MobileServiceSQLiteStore = new MobileServiceSQLiteStore(_LocalDbName);

                _MobileServiceSQLiteStore.DefineTable<ColorPreference>();

                _ColorPrefTable = _MobileService.GetSyncTable<ColorPreference>();

                _SyncHandler = new SyncHandler<ColorPreference>();

                await _MobileService.SyncContext.InitializeAsync(_MobileServiceSQLiteStore, _SyncHandler).ConfigureAwait(false);

                _IsInitialized = true;

                return _IsInitialized;
            }, false).ConfigureAwait(false);
        }

        /// <summary>
        /// Syncs the items.
        /// </summary>
        /// <returns>The items are synced.</returns>
        async Task<bool> SyncItemsAsync()
        {
            return await Execute(async () =>
            {
                await InitializeAsync().ConfigureAwait(false);
                // PushAsync() has been omitted here because the _MobileService.SyncContext automatically calls PushAsync() before PullAsync() if it sees pending changes in the context. (Frequently misunderstood feature of the Azure App Service SDK)
                try
                {
                    await _ColorPrefTable.PullAsync(null, _ColorPrefTable.Where(x => true)).ConfigureAwait(false);
                }
                catch (Exception e)
                {

                    var temp = e.Message;
                }
                return true;
            }, false);
        }

        /// <summary>
        /// Resets the local store.
        /// </summary>
        async Task ResetLocalStoreAsync()
        {
            _ColorPrefTable = null;

            // On UWP, it's necessary to Dispose() and nullify the MobileServiceSQLiteStore before 
            // trying to delete the database file, otherwise an access exception will occur
            // because of an open file handle. It's okay to do for iOS and Android as well, but not necessary.
            _MobileServiceSQLiteStore?.Dispose();
            _MobileServiceSQLiteStore = null;

            _IsInitialized = false;
        }

        #endregion

    }
}
