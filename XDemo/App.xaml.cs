﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XDemo.Views;
using XDemo.ViewModels;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace XDemo
{
    public partial class App : Application
    {
        static NavigationPage _NavPage;

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new XDemo.MainPage());

            _NavPage = (NavigationPage)MainPage;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public static bool IsLoggedIn
        {
            get { return !string.IsNullOrWhiteSpace(_Token); }
        }

        static string _Token;
        public static string Token
        {
            get { return _Token; }
        }

        public static void SaveToken(string token)
        {
            _Token = token;
        }

        static string _ConsumerKey;
        public static string ConsumerKey
        {
            get { return _ConsumerKey; }
        }

        public static void SaveConsumerKey(string consumerKey)
        {
            _ConsumerKey = consumerKey;
        }

        static string _ConsumerSecret;
        public static string ConsumerSecret
        {
            get { return _ConsumerSecret; }
        }

        public static void SaveConsumerSecret(string consumerSecret)
        {
            _ConsumerSecret = consumerSecret;
        }

        static string _User;
        public static string User
        {
            get { return _User; }
        }

        public static void SaveUser(string user)
        {
            _User = user;
        }

        static string _UserId;
        public static string UserId
        {
            get { return _UserId; }
        }

        public static void SaveUserId(string userId)
        {
            _UserId = userId;
        }

        public static Action SuccessfulLoginAction
        {
            get
            {
                return new Action(async () => {
                    await _NavPage.Navigation.PopModalAsync();

                    await _NavPage.Navigation.PushAsync(new TwitterProfilePage
                    {
                        Title = "Twitter Profile",
                        BindingContext = new TwitterProfileViewModel(User, UserId, ConsumerKey, ConsumerSecret)
                    });

                });
            }
        }

    }
}
