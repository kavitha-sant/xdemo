﻿using MvvmHelpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XDemo.Models;
using PCLCrypto;
using static PCLCrypto.WinRTCrypto;
using System.Net;

namespace XDemo.ViewModels
{
    public class TwitterProfileViewModel : INotifyPropertyChanged
    {
        public string UserProfileImage { get; set; }
        public string UserBannerImage { get; set; }
        public string UserCreated { get; set; }

        public string TwitterUser { get; set; }
        public string TwitterUserId { get; set; }
        public string TwitterconsumerKey { get; set; }
        public string TwitterconsumerSecret { get; set; }
        private static HttpClient _httpClient = new HttpClient();
        private static string BearerToken;

        public event PropertyChangedEventHandler PropertyChanged;

        public TwitterProfileViewModel(string User, string UserId, string consumerKey, string consumerSecret)
        {
            TwitterUser = User;
            TwitterUserId = UserId;
            TwitterconsumerKey = consumerKey;
            TwitterconsumerSecret = consumerSecret;
        }

        public async Task GetTwitterImages()
        {
            var userProfileUrl = $"https://api.twitter.com/1.1/users/show.json?screen_name={TwitterUser}";
            var data = new Dictionary<string, string>();

            var encodedConsumerKey = WebUtility.UrlEncode(TwitterconsumerKey);
            var encodedConsumerSecret = WebUtility.UrlEncode(TwitterconsumerSecret);

            var bearerTokenCred = string.Format($"{encodedConsumerKey}:{encodedConsumerSecret}");
            var base64Cred = CryptographicBuffer.EncodeToBase64String(CryptographicBuffer.ConvertStringToBinary(bearerTokenCred, Encoding.UTF8));

            _httpClient.DefaultRequestHeaders.Remove("Authorization");
            _httpClient.DefaultRequestHeaders.Add("Authorization", string.Format($"Basic {base64Cred}"));

            data.Add("grant_type", "client_credentials");

            var formData = new FormUrlEncodedContent(data);

            var response = await _httpClient.PostAsync("https://api.twitter.com/oauth2/token", formData);

            if (response.IsSuccessStatusCode)
            {
                var postResponsestring = await response.Content.ReadAsStringAsync();
                dynamic result = JsonConvert.DeserializeObject(postResponsestring);

                if(result.token_type == "bearer")
                {
                    BearerToken = result.access_token;

                    _httpClient.DefaultRequestHeaders.Remove("Authorization");
                    _httpClient.DefaultRequestHeaders.Add("Authorization", string.Format($"Bearer {BearerToken}"));

                    response = await _httpClient.GetAsync(userProfileUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        var getResponsestring = await response.Content.ReadAsStringAsync();
                        dynamic userProfileResult = JsonConvert.DeserializeObject(getResponsestring);

                        UserProfileImage = userProfileResult.profile_image_url;
                        UserBannerImage = userProfileResult.profile_banner_url;
                        UserCreated = userProfileResult.created_at;

                        OnPropertyChanged("UserProfileImage");
                        OnPropertyChanged("UserBannerImage");
                        OnPropertyChanged("UserCreated");
                    }
                }
            }
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var changed = PropertyChanged;
            if (changed != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
