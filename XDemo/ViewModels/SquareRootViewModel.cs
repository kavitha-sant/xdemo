﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace XDemo.ViewModels
{
    public class SquareRootViewModel : INotifyPropertyChanged
    {
        public int Number { get; set; }
        public double SquareRootResult { get; set; }
        public ICommand SquareRootCommand { get; private set; }
 
	    public SquareRootViewModel()
        {
            SquareRootCommand = new Command(CalculateSquareRoot);
	    }

        public event PropertyChangedEventHandler PropertyChanged;

        void CalculateSquareRoot()
        {
            SquareRootResult = Math.Sqrt(Number);
            OnPropertyChanged("SquareRootResult");
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var changed = PropertyChanged;
            if (changed != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
