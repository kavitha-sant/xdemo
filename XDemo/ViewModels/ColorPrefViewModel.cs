﻿using MvvmHelpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using XDemo.Models;

namespace XDemo.ViewModels
{
    public class ColorPrefViewModel : INotifyPropertyChanged
    {
        AzureColorPrefDataSource _ColorPrefDataSource;
        public ICommand SquareRootCommand { get; private set; }

        public string NewName { get; set; }
        public string NewColor { get; set; }

        ObservableRangeCollection<ColorPreference> _ColorPreferences;
        public ObservableRangeCollection<ColorPreference> ColorPreferences
        {
            get { return _ColorPreferences ?? (_ColorPreferences = new ObservableRangeCollection<ColorPreference>()); }
            set
            {
                _ColorPreferences = value;
                OnPropertyChanged("ColorPreferences");
            }
        }

        public ColorPrefViewModel()
        {
            _ColorPrefDataSource = new AzureColorPrefDataSource();

        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var changed = PropertyChanged;
            if (changed != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Command to load ColorPrefs
        /// </summary>

        Command _LoadColorPrefsCommand;
        public Command LoadColorPrefsCommand
        {
            get { return _LoadColorPrefsCommand ?? (_LoadColorPrefsCommand = new Command(async () => await ExecuteLoadColorPrefsCommandAsync())); }
        }

        public async Task ExecuteLoadColorPrefsCommandAsync()
        {
            LoadColorPrefsCommand.ChangeCanExecute();

            ColorPreferences = new ObservableRangeCollection<ColorPreference>( await _ColorPrefDataSource.GetItemsAsync());

            LoadColorPrefsCommand.ChangeCanExecute();
        }

        /// <summary>
        /// Command to add ColorPref
        /// </summary>

        Command _AddColorPrefCommand;
        public Command AddColorPrefCommand
        {
            get { return _AddColorPrefCommand ?? (_AddColorPrefCommand = new Command(async () => await ExecuteAddColorPrefCommandAsync())); }
        }

        public async Task ExecuteAddColorPrefCommandAsync()
        {
            AddColorPrefCommand.ChangeCanExecute();

            await _ColorPrefDataSource.AddItemAsync(new ColorPreference { Name = NewName, Color = NewColor });

            ColorPreferences = new ObservableRangeCollection<ColorPreference>(await _ColorPrefDataSource.GetItemsAsync());
            OnPropertyChanged("ColorPreferences");

            NewName = string.Empty;
            NewColor = string.Empty;
            OnPropertyChanged("NewName");
            OnPropertyChanged("NewColor");

            AddColorPrefCommand.ChangeCanExecute();
        }

        /// <summary>
        /// Command to delete ColorPref
        /// </summary>

        Command _DeleteColorPrefCommand;
        public Command DeleteColorPrefCommand
        {
            get { return _DeleteColorPrefCommand ?? (_DeleteColorPrefCommand = new Command<ColorPreference>(async (colorPref) => await ExecuteDeleteColorPrefCommandAsync(colorPref))); }
        }

        public async Task ExecuteDeleteColorPrefCommandAsync(ColorPreference colorPref)
        {
            DeleteColorPrefCommand.ChangeCanExecute();

            await _ColorPrefDataSource.RemoveItemAsync(colorPref);

            ColorPreferences = new ObservableRangeCollection<ColorPreference>(await _ColorPrefDataSource.GetItemsAsync());
            OnPropertyChanged("ColorPreferences");

            DeleteColorPrefCommand.ChangeCanExecute();
        }

    }
}
