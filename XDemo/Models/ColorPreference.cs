﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDemo.Models
{
    public class ColorPreference : ObservableEntityData
    {
        public string Name { get; set; }

        public string Color { get; set; }
    }
}
